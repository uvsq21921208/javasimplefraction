package Fraction;

public final class Fraction {

	private int num;
	private int denum;
	public static final Fraction UN = new Fraction(1,1);
	public static final Fraction zero = new Fraction(0,1);
	
	public Fraction(int num, int denum){
		this.setDenum(denum);
		this.setNum(num);
	}
	public Fraction(int num){
		this.setDenum(1);
		this.setNum(num);
	}
	public Fraction(){
		this.setDenum(1);
		this.setNum(0);
	}
	public int getDenum() {
		return denum;
	}
	public void setDenum(int denum) {
		this.denum = denum;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public double getFloatValue() {
		return (float) this.num/this.denum;
	}
	
	public Fraction addition(Fraction a) {
		
		if (this.getDenum() == a.getDenum()) {
				return new Fraction(a.getNum()+this.getNum(), a.getDenum());
		}else {
		int d = a.getDenum()*this.getDenum();
		this.setNum(a.getDenum()*this.getNum());
		a.setNum(this.getDenum()*a.getNum());
		this.setDenum(d);
		a.setDenum(d);
		return new Fraction(a.getNum()+this.getNum(), a.getDenum());
		}
		
	}
	
	public boolean equals(Fraction a) {
		
		
		
		
		Fraction c = new Fraction(this.getNum(), this.getDenum());
		Fraction f = new Fraction(a.getNum(), a.getDenum());
		
		int d = f.getDenum()*this.getDenum();
		c.setNum(f.getDenum()*c.getNum());
		f.setNum(c.getDenum()*f.getNum());
		this.setDenum(d);
		a.setDenum(d);
		
		
		return c.getNum() == f.getNum() && c.getDenum() == f.getDenum();
		
		
		
	}
}